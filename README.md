# mysql高级

#### 介绍
mysql高级笔记

Mysql连接
jdbc:mysql://localhost:3306/demo?useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8&zeroDateTimeBehavior=convertToNull

#### 使用说明

    P1 day01-02. MySQL 高级 - Linux上安装MySQL
    P2 day01-03. MySQL 高级 - 启动及登录MySQL
    P3 day01-04. MySQL 高级 - 索引 - 概述
    P4 day01-05. MySQL 高级 - 索引 - 优势和劣势
    P5 day01-06. MySQL 高级 - 索引 - 数据结构
    P6 day01-07. MySQL 高级 - 索引 - 数据结构 - BTREE
    P7 day01-08. MySQL 高级 - 索引 - 数据结构 - B+TREE
    P8 day01-09. MySQL 高级 - 索引 - 索引分类
    P9 day01-10. MySQL 高级 - 索引 - 索引语法
    P10 day01-11. MySQL 高级 - 索引 - 索引设计原则
    P11 day01-12. MySQL 高级 - 视图 - 概述
    P12 day01-13. MySQL 高级 - 视图 - 创建及修改视图
    P13 day01-14. MySQL 高级 - 视图 - 查看及删除视图
    P14 day01-15. MySQL 高级 - 存储过程 - 概述
    P15 day01-16. MySQL 高级 - 存储过程 - 创建调用查询删除语法
    P16 day01-17. MySQL 高级 - 存储过程 - 语法 - 变量
    P17 day01-18. MySQL 高级 - 存储过程 - 语法 - if判断
    P18 day01-19. MySQL 高级 - 存储过程 - 语法 - 输入参数
    P19 day01-20. MySQL 高级 - 存储过程 - 语法 - 输出参数
    P20 day01-21. MySQL 高级 - 存储过程 - 语法 - case结构
    P21 day01-22. MySQL 高级 - 存储过程 - 语法 - while循环
    P22 day01-23. MySQL 高级 - 存储过程 - 语法 - repeat循环
    P23 day01-24. MySQL 高级 - 存储过程 - 语法 - loop循环
    P24 day01-25. MySQL 高级 - 存储过程 - 语法 - 游标介绍
    P25 day01-26. MySQL 高级 - 存储过程 - 语法 - 游标基本操作
    P26 day01-27. MySQL 高级 - 存储过程 - 语法 - 循环获取游标
    P27 day01-28. MySQL 高级 - 存储过程 - 函数
    P28 day01-29. MySQL 高级 - 触发器 - 介绍
    P29 day01-30. MySQL 高级 - 触发器 - 创建及应用
    P30 day01-31. MySQL 高级 - 触发器 - 查看及删除
    P31 day02-02. MySQL高级 - 体系结构
    P32 day02-03. MySQL高级 - 存储引擎 - 概述
    P33 day02-04. MySQL高级 - 存储引擎 - 特性
    P34 day02-05. MySQL高级 - 存储引擎 - InnoDB特性
    P35 day02-06. MySQL高级 - 存储引擎 - MyISAM特性
    P36 day02-07. MySQL高级 - 存储引擎 - Memory与Merge特性
    P37 day02-08. MySQL高级 - 存储引擎 - 选择原则
    P38 day02-09. MySQL高级 - 优化SQL步骤 - SQL执行频率
    P39 day02-10. MySQL高级 - 优化SQL步骤 - 定位低效SQL
    P40 day02-11. MySQL高级 - 优化SQL步骤 - explain指令介绍
    P41 day02-12. MySQL高级 - 优化SQL步骤 - explain之id
    P42 day02-13. MySQL高级 - 优化SQL步骤 - explain之select_type
    P43 day02-14. MySQL高级 - 优化SQL步骤 - explain之table type
    P44 day02-15. MySQL高级 - 优化SQL步骤 - explain之key rows extra
    P45 day02-16. MySQL高级 - 优化SQL步骤 - show profile
    P46 day02-17. MySQL高级 - 优化SQL步骤 - trace工具
    P47 day02-18. MySQL高级 - 索引的使用 - 验证索引提升查询效率
    P48 day02-19. MySQL高级 - 索引的使用 - 全值匹配
    P49 day02-20. MySQL高级 - 索引的使用 - 最左前缀法则
    P50 day02-21. MySQL高级 - 索引的使用 - 索引失效情况(范围查询，字段运算)
    P51 day02-22. MySQL高级 - 索引的使用 - 覆盖索引
    P52 day02-23. MySQL高级 - 索引的使用 - or索引失效情况
    P53 day02-24. MySQL高级 - 索引的使用 - like模糊匹配
    P54 day02-25. MySQL高级 - 索引的使用 - 全表扫描更快
    P55 day02-26. MySQL高级 - 索引的使用 - NULL值的判定
    P56 day02-27. MySQL高级 - 索引的使用 - in和not in
    P57 day02-28. MySQL高级 - 索引的使用 - 单列索引与复合索引选择
    P58 day02-29. MySQL高级 - 索引的使用 - 查看索引使用情况
    P59 day02-30. MySQL高级 - SQL优化 - 大批量插入数据
    P60 day02-31. MySQL高级 - SQL优化 - insert优化
    P61 day02-32. MySQL高级 - SQL优化 - orderby 优化
    P62 day02-33. MySQL高级 - SQL优化 - group by 优化
    P63 day02-34. MySQL高级 - SQL优化 - 子查询优化
    P64 day02-35. MySQL高级 - SQL优化 - or优化
    P65 day02-36. MySQL高级 - SQL优化 - limit优化
    P66 day02-37. MySQL高级 - SQL优化 - 索引提示
    P67 day03-02. MySQL高级 - 应用优化
    P68 day03-03. MySQL高级 - 查询缓存 - 概述及流程
    P69 day03-04. MySQL高级 - 查询缓存 - 配置参数
    P70 day03-05. MySQL高级 - 查询缓存 - 开启查询缓存
    P71 day03-06. MySQL高级 - 查询缓存 - SELECT选项
    P72 day03-07. MySQL高级 - 查询缓存 - 失效场景
    P73 day03-08. MySQL高级 - 内存优化 - 优化原则
    P74 day03-09. MySQL高级 - 内存优化 - MyISAM内存优化
    P75 day03-10. MySQL高级 - 内存优化 - InnoDB内存优化
    P76 day03-11. MySQL高级 - 并发参数调整
    P77 day03-12. MySQL高级 - 锁 - 锁的概述及分类
    P78 day03-13. MySQL高级 - 锁 - MySQL对锁的支持
    P79 day03-14. MySQL高级 - 锁 - MyISAM表锁 - 读锁
    P80 day03-15. MySQL高级 - 锁 - MyISAM表锁 - 写锁
    P81 day03-16. MySQL高级 - 锁 - MyISAM表锁 - 小结
    P82 day03-17. MySQL高级 - 锁 - MyISAM表锁 - 查看锁争用情况
    P83 day03-18. MySQL高级 - 锁 - InnoDB行锁 - 介绍及背景知识
    P84 day03-19. MySQL高级 - 锁 - InnoDB行锁 - 基本演示
    P85 day03-20. MySQL高级 - 锁 - InnoDB行锁 - 行锁升级为表锁
    P86 day03-21. MySQL高级 - 锁 - InnoDB行锁 - 间隙锁危害
    P87 day03-22. MySQL高级 - 锁 - InnoDB行锁 - 争用情况查看
    P88 day03-23. MySQL高级 - 锁 - InnoDB行锁 - 总结
    P89 day03-24. MySQL高级 - SQL技巧 - SQL执行顺序及正则表达式
    P90 day03-25. MySQL高级 - SQL技巧 - 数字函数与字符串函数
    P91 day03-26. MySQL高级 - SQL技巧 -日期函数与聚合函数
    P92 day04-02. MySQL高级 - 常用工具 - mysql
    P93 day04-03. MySQL高级 - 常用工具 - mysqladmin
    P94 day04-04. MySQL高级 - 常用工具 - mysqlbinlog与mysqldump
    P95 day04-05. MySQL高级 - 常用工具 - mysqlimport与source
    P96 day04-06. MySQL高级 - 常用工具 - mysqlshow
    P97 day04-07. MySQL高级 - 日志 - 错误日志
    P98 day04-08. MySQL高级 - 日志 - 二进制日志(statement)
    P99 day04-09. MySQL高级 - 日志 - 二进制日志(row及日志删除)
    P100 day04-10. MySQL高级 - 日志 - 查询日志
    P101 day04-11. MySQL高级 - 日志 - 慢查询日志
    P102 day04-12. MySQL高级 - 复制 - 原理
    P103 day04-13. MySQL高级 - 复制 - 集群搭建
    P104 day04-14. MySQL高级 - 案例 - 需求及环境准备
    P105 day04-15. MySQL高级 - 案例 - 基本工程导入
    P106 day04-16. MySQL高级 - 案例 - AOP记录日志
    P107 day04-17. MySQL高级 - 案例 - 日志查询后端 - mapper接口
    P108 day04-18. MySQL高级 - 案例 - 日志查询后端 - Service&Controller
    P109 day04-19. MySQL高级 - 案例 - 日志查询 - 前端
    P110 day04-20. MySQL高级 - 案例 - 系统性能优化分析
    P111 day04-21. MySQL高级 - 案例 - 系统性能优化 - 分页优化
    P112 day04-22. MySQL高级 - 案例 - 系统性能优化 - 索引优化
    P113 day04-23. MySQL高级 - 案例 - 系统性能优化 - 读写分离概述
    P114 day04-24. MySQL高级 - 案例 - 系统性能优化 - 数据源配置
    P115 day04-25. MySQL高级 - 案例 - 系统性能优化 - AOP切换数据源
    P116 day04-26. MySQL高级 - 案例 - 系统性能优化 - AOP切换数据源 - 测试
    P117 day04-27. MySQL高级 - 案例 - 系统性能优化 - AOP切换数据源 - 原理解析
    P118 day04-28. MySQL高级 - 案例 - 系统性能优化 - 应用优化 
